import re
import urllib

from . import bt

# Note, the \n\ is to prevent trailing whitespace from being stripped by
# people's editors. It is there intentionally.

DESC_TEMPLATE = """## Submitted by {submitter}  \n\
{assigned_to}
**[Link to original bugzilla bug (#{id})]\
({link_url}{id})**  \n\
{version_ph}\
{platform_ph}\
{op_sys_ph}
## Description
{body}

{dependencies}
"""

DEPENDENCIES_TEMPLATE = """{depends_on}
{blocks}
{see_also}
"""

COMMENT_TEMPLATE = """:{emoji}: **{author}** {action}:
{body}  \n\
{attachment}
"""

ATTACHMENT_TEMPLATE = """  \n\
{obsolete}**{kind} {atid}**{obsolete}, "{summary}":  \n\
{markdown}
"""

MIGR_TEMPLATE = """-- GitLab Migration Automatic Message --

This bug has been migrated to {instance}'s GitLab instance and has been \
closed from further activity.

You can subscribe and participate further through the new bug through this \
link to our GitLab instance: {gitlab_url}.
"""

# generate a link to original bugzilla instance
def _bugzilla_url(instance_base, bugid):
    url = '{instance_base}/show_bug.cgi?id={bugid}'
    return url.format(instance_base=instance_base, bugid=bugid)

# within gitlab, #bugid is enough
def _markdown_url(instance_base, bugid):
    url = '#{bugid}'
    return url.format(bugid=bugid)

# replace "Bug ID"/"bug ID" by "Bug #ID"/"bug #ID"
def _filter_links(instance_base, text, hg2git):

    text = re.sub(r'([Bb]ug) ([0-9]+)', '\\1 #\\2', text)
    # Prevent spurious links to other GitLab issues
    text = re.sub(r'([Cc]omment) #([0-9]+)', '\\1 \\2', text)
    # Quote stack traces as preformatted text
    text = bt.quote_stack_traces(text)
    # translate PR

    def pr_sub(match):
        res = "PR-{}".format(match.group(1))
        print("SUB PR {} by {}".format(match.group(0),res))
        return res

    text = re.sub(r'PR ([0-9]+)',pr_sub,text) # we use pr_sub instead of 'PR-\\1' for logging/debug purpose
    text = re.sub(r'https://bitbucket.org/eigen/eigen/pull-requests/([0-9]+)[^\s\)]*',pr_sub,text)

    # translate hg hashes to git
    def hash_sub(match):
        hg_hash = match.group(1)
        # print("check: {}".format(hg_hash))
        git_hash = hg2git.git_hash(hg_hash)
        if git_hash:
            print("SUB HASH {} by {}".format(match.group(0),git_hash))
            return git_hash
        else:
            return match.group(0)

    text = re.sub(r"https://bitbucket.org/eigen/eigen/commits/([0-9a-fA-F]{6,50})/?",hash_sub,text)
    text = re.sub(r"https://bitbucket.org/eigen/eigen/changeset/([0-9a-fA-F]{6,50})/?",hash_sub,text)
    text = re.sub(r"\b([0-9a-fA-F]{6,50})\b",hash_sub,text)

    # Replace "<" and ">" of XML-like tags by &lt;/&gt;
    # They would otherwise be stripped by GitLab 
    tags_outside_quotes = re.compile(r"""
        (
            ^[^`]*                    # An initial string with no backticks
            (?:                       # Then zero or more of...
                (?:
                    \n```.*?\n```     # A matched pair of triple backticks
                |
                    `[^`]+`           # Or a matched pair of backticks
                )
                [^`]*?                # Followed by a string with no backticks
            )*?                       # These are skipped over before we find
        )
        \<(\/?[a-zA-Z0-9_="' -]*?)\>  # ...an XML-like tag
        """, re.VERBOSE)
    nsubs = 1
    while nsubs > 0:
        # Matches may overlap, so we have to keep substituting until none left
        text, nsubs = tags_outside_quotes.subn('\\1&lt;\\2&gt;', text)

    # Add "  " at the end of every lines to preserve formating
    text = re.sub(r'$','  ',text,0,re.MULTILINE)
    return text

def _body_to_markdown_quote(body):
    if not body:
        return '\n'
    # gael, no need to add additional quotes.
    # return ">>>\n{}\n>>>\n".format(body)
    return "\n{}\n\n".format(body)


def render_issue_description(
        instance_base, bug, text, user_cache, hg2git, importing_address=None,
        bug_url_function=_markdown_url):
    if not text:
        text = ""
    if not importing_address:
        importing_address = "{}/show_bug.cgi?id=".format(instance_base)

    assigned_to = ""
    assignee = user_cache[bug.assigned_to]
    if assignee is not None:
        assigned_to = "Assigned to **{}**  \n".format(assignee.display_name())

    deps = ""
    if bug.depends_on:
        deps += "### Depends on\n"
        for bugid in bug.depends_on:
            deps += "#{} ".format(bugid)

    blocks = ""
    if bug.blocks:
        blocks += "### Blocking\n"
        for bugid in bug.blocks:
            blocks += "#{} ".format(bugid)

    see_also = ""
    if bug.see_also:
        see_also += "### See also\n"
        for url in bug.see_also:
            is_bz = False
            try:
                bug_url = urllib.parse.urlparse(url)
                query = urllib.parse.parse_qs(bug_url.query)
            except Exception as e:
                pass
            else:
                ids = query.get('id', [])
                # XXX
                if bug_url.netloc == 'eigen.tuxfamily.org' and ids:
                    is_bz = True
                    see_also += "  * Bug #{}\n".format(ids[0])

            if not is_bz:
                see_also += "  * {}\n".format(url)

    dependencies = DEPENDENCIES_TEMPLATE.format(depends_on=deps,
                                                blocks=blocks,
                                                see_also=see_also)

    body = _filter_links(instance_base, text, hg2git)

    version_text = ''
    if bug.version and bug.version not in ('master', 'unspecified'):
        version_text = '**Version**: {}  \n'.format(bug.version)

    platform_text = ''
    if bug.platform and bug.platform not in ('All','Other'):
        platform_text = '**Platform**: {}  \n'.format(bug.platform)

    op_sys_text = ''
    if bug.op_sys and bug.op_sys not in ('All','Other'):
        platform_text = '**Operating system**: {}  \n'.format(bug.op_sys)

    try:
        submitter = user_cache[bug.creator].display_name()
    except AttributeError:
        submitter = 'an unknown user'

    return DESC_TEMPLATE.format(
        link_url=importing_address, submitter=submitter,
        assigned_to=assigned_to, id=bug.id,
        version_ph=version_text,
        platform_ph=platform_text,
        op_sys_ph=op_sys_text,
        body=body,
        dependencies=dependencies)


def render_bugzilla_migration_comment(instance, gl_issue):
    return MIGR_TEMPLATE.format(instance=instance, gitlab_url=gl_issue.web_url)


def render_comment(instance_base, emoji, author, action, body, attachment, hg2git,
                   bug_url_function=_markdown_url):

    if body:
        body = _filter_links(instance_base, body, hg2git)

    body = _body_to_markdown_quote(body)

    return COMMENT_TEMPLATE.format(
        emoji=emoji, author=author, action=action,
        body=body,
        attachment=attachment)


def render_attachment(atid, metadata, gitlab_ret):
    return ATTACHMENT_TEMPLATE.format(
        atid=atid,
        kind='Patch' if metadata['is_patch'] else 'Attachment',
        obsolete='~~' if metadata['is_obsolete'] else '',
        summary=metadata['summary'],
        markdown=gitlab_ret['markdown'])
