#    Copyright 2017 Alberto Ruiz <aruiz@gnome.org>
#    Copyright 2017 Philip Chimento <philip.chimento@gmail.com>
#    Copyright 2019 Gael Guenenbaud <gael.guennebaud@inria.fr>

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import itertools
import os
import re
import sys
import urllib.parse

import bugzilla
import gitlab
import time

import inspect

from . import common, milestones, template, users

# The following maps are used to convert the respective bugzilla fields,
# as GitLab labels.
# Commented lines means that the respective field value will be ignored.

COMPONENT_MAP = {
    'Cholesky':'cholesky',
    'Core - expression templates':'expression templates',
    'Core - general':'general',
    'Core - matrix products':'products',
    'Core - vectorization':'vectorization',
    'Documentation':'documentation',
    'Eigenvalues':'eigenvalues',
    'General':'general',
    'Geometry':'geometry',
    'Householder':'householder',
    'Interoperability':'interoperability',
    'Jacobi':'jacobi',
    'LU':'LU',
    'QR':'QR',
    'Sparse':'sparse',
    'SVD':'SVD',
    'Tensor':'tensor',
    'Tests':'tests',
    'Unsupported modules':'unsupported'
}

# Cholesky    Nobody LLT and LDLT
# Core - expression templates Nobody Bugs with expression trees, bad temporaries, aliasing...
# Core - general  Nobody Core bugs that don't fit in other categories
# Core - matrix products  Nobody matrix-matrix and matrix-vector multiplication
# Core - vectorization    Nobody SSE/AVX/NEON/AltiVec vectorization and alignment issues
# Documentation   Nobody Eigen documentation
# Eigenvalues Nobody Eigenvalues module
# General Nobody Eigen bugs that don't fit in other categories
# Geometry    Nobody Geometry module
# Householder Nobody Householder module
# Interoperability    Nobody Interoperability with other libraries, such as STL
# Jacobi  Nobody Jacobi module. For JacobiSVD, see SVD.
# LU  Nobody LU module. Also inverse and determinant.
# QR  Nobody QR module
# Sparse  Nobody Sparse module
# SVD Nobody SVD module
# Tensor  Nobody Tensor module (unsupported)
# Tests   Nobody Unit tests
# Unsupported modules Nobody All unsupported modules go there

KEYWORD_MAP = {
    "accuracy":         "accuracy",
    "alignment":        "alignment",
    "bloat":            "bloat",
    "BuildSystem":      "build system",
    "Compatibility":    "compatibility",
    "crash":            "crash",
    "Documentation":    "documentation",
    "JuniorJob":        "junior job",
    "performance":      "performance",
    "test-needed":      "test needed",
    "wrong-result":     "wrong result"
}

# accuracy    Numerical accuracy/stability bugs
# alignment   Alignment-related bugs
# bloat   Executable code bloat bugs
# BuildSystem Any (cmake related) problems building Eigen tests or binary libraries.
# Compatibility   Fixes that are required to handle different platforms/compilers, especially with non-standard behavior.
# crash   Crasher bugs.
# Documentation   The behavior of a function needs to be documented better.
# JuniorJob   This means that handling this task does not require deep knowledge of Eigen's internals. This is a good opportunity to start contributing to Eigen. Search  49
# performance Poor performance bugs.
# test-needed A unit-test needs to be written to cover this bug, even if it's already been marked as resolved. Remove this tag once it's written.
# wrong-result Bugs resulting in plain wrong results

SEVERITY_MAP = {
    'Crash':                'crash',
    'Compilation Problem':  'compilation problem',
    'Failed Unit Test':     'failed unit test',
    'Wrong Result':         'wrong result',
    'Accuracy Problem':     'accuracy',
    'Performance Problem':  'performance',
    'Unit Test Required':   'unit test required',
    'Feature Request':      'feature request',
    'API Change':           'API change',
    'Internal Design':      'internal design',
    'Optimization':         'optimisation',
    'Documentation':        'documentation',
    # 'Unknown':            '',
    'critical':             'priority++',
    'major':                'priority+',
    'minor':                'priority--',
    'enhancement':          'enhancement'
}

PRIORITY_MAP = {
    'Highest':  'priority++',
    'High':     'priority+',
    # 'Normal': '',
    'Low':      'priority-',
    'Lowest':   'priority--'
}

PRIORITY2WEIGHT = {
    'Highest':  9,
    'High':     7,
    'Normal':   -1,
    'Low':      3,
    'Lowest':   1
}

STATUS_MAP = {
    'UNCONFIRMED':      'unconfirmed',
    'NEW':              'new',
    'CONFIRMED':        'confirmed',
    # 'ASSIGNED':       '',
    'DECISIONNEEDED':   'decision needed',
    'REVIEWNEEDED':     'review needed',
    'VERIFIED':         'verified'
    # 'REOPENED':       '',
    # 'CLOSED':         '',
    # 'RESOLVED':       ''
}

def spam_workaround(text):
    text = re.sub(r'\_','&lowbar;',text)
    text = re.sub(r'\<','&lt;',text)
    text = re.sub(r'\>','&gt;',text)
    text = re.sub(r'/var/jenkins_home/workspace/neon_PR-125-SEMONSAXNP4DJH6WB6FH3YM56CLGTVEOFZ5NHXEJWRZCJENGXRBQ@4/build/eigen3/src/eigen3','',text)
    text = re.sub(r'eigen-eigen-920fc730b593/','',text)
    text = re.sub(r'/export/home/orion/fedora/eigen3/eigen-eigen-ffa86ffb5570/','',text)
    text = re.sub(r'/sw/build.build/eigen3-3.2.0-1/eigen-eigen-ffa86ffb5570','',text)
    return text

def _gitlab_url(instance_base, bugid):
    url = '{instance_base}/issues/{bugid}'
    return url.format(instance_base=instance_base, bugid=bugid)

def processbug(bgo, bzurl, instance, resolution, target, user_cache,
               milestone_cache, bzbug, hg2git):
    print("Processing bug #%d: %s" % (bzbug.id, bzbug.summary))
    # For the record, some useful attributes of bzbug:
    #   bzbug.cc, .id, .summary, .creator, .creationtime, .target_milestone, .blocks, .depends_on, .see_also, .assigned_to

    def get_attachments_metadata(self):
        # pylint: disable=protected-access
        proxy = self.bugzilla._proxy
        # pylint: enable=protected-access

        if "attachments" in self.__dict__:
            attachments = self.attachments
        else:
            rawret = proxy.Bug.attachments(
                {"ids": [self.bug_id], "exclude_fields": ["data"]})
            attachments = rawret["bugs"][str(self.bug_id)]

        index = {}
        for at in attachments:
            atid = at.pop('id')
            index[atid] = at
        return index

    def gitlab_upload_file(target, filename, f, sudo=None):
        url = "{}api/v4/projects/{}/uploads".format(target.gl_url,
                                                    target.get_project().id)
        target.gl.session.headers = {"PRIVATE-TOKEN": target.token}
        #if sudo:
            #target.gl.session.headers['sudo'] = '{}'.format(sudo)
        ret = target.gl.session.post(url, files={
            'file': (urllib.parse.quote(filename), f)
        })
        if ret.status_code != 201:
            raise Exception("Could not upload file: {}".format(ret.text))
        return ret.json()

    def migrate_attachment(comment, metadata):
        atid = comment['attachment_id']

        if 'author' in comment:
            author = user_cache[comment['author']]
        else:
            author = user_cache[comment['creator']]
        filename = metadata[atid]['file_name']
        print("    Attachment {} found, migrating".format(filename))
        attfile = bgo.openattachment(atid)
        ret = gitlab_upload_file(target, filename, attfile)#, sudo=author.id)

        return template.render_attachment(atid, metadata[atid], ret)

    def remove_first_lines(text, numlines):
        return '\n'.join(text.split('\n')[numlines:])

    def convert_review_comments_to_markdown(text):
        paragraphs = text.split('\n\n')
        converted_paragraphs = []
        for paragraph in paragraphs:
            # Quick check if this is a diff block
            if paragraph[:2] not in ('::', '@@'):
                converted_paragraphs.append(paragraph)
                continue

            # Slow check if this is a diff block
            lines = paragraph.split('\n')
            if not all([line[0] in ':@+- ' for line in lines]):
                converted_paragraphs.append(paragraph)
                continue

            converted_paragraphs.append('```diff\n{}\n```'.format(paragraph))

        return '\n\n'.join(converted_paragraphs)

    def analyze_bugzilla_comment(comment, attachment_metadata):
        body = comment['text']

        if re.match(r'Created attachment ([0-9]+)\n', body):
            # Remove two lines of attachment description and blank line
            body = remove_first_lines(body, 3)
            if attachment_metadata[comment['attachment_id']]['is_patch']:
                return 'hammer_and_wrench', 'submitted a patch', body
            return 'paperclip', 'uploaded an attachment', body

        match = re.match(r'Review of attachment ([0-9]+):\n', body)
        if match:
            body = remove_first_lines(body, 2)
            body = convert_review_comments_to_markdown(body)
            return 'mag', 'reviewed patch {}'.format(match.group(1)), body

        match = re.match(r'Comment on attachment ([0-9]+)\n', body)
        if match:
            body = remove_first_lines(body, 3)

            # git-bz will push a single commit as a comment on the patch
            if re.match(r'Attachment [0-9]+ pushed as [0-9a-f]+ -', body):
                return 'arrow_heading_up', 'committed a patch', body

            kind = 'attachment'
            if attachment_metadata[comment['attachment_id']]['is_patch']:
                kind = 'patch'
            action = 'commented on {} {}'.format(kind, match.group(1))
            return 'speech_balloon', action, body

        # git-bz pushing multiple commits is just a plain comment. Add
        # formatting so that the lines don't run together
        if re.match(r'Attachment [0-9]+ pushed as [0-9a-f]+ -', body):
            body = body.replace('\n', '  \n')
            return 'arrow_heading_up', 'committed some patches', body

        if re.match(r'\*\*\* Bug [0-9]+ has been marked as a duplicate of '
                    'this bug. \*\*\*', body):
            return 'link', 'closed a related bug', body

        return 'speech_balloon', 'said', body

    try :
        issue = target.get_project().issues.get(bzbug.id)
        # issue.description = issue.description
        # issue.save()
        print(("-> entry {} already exists, nothing to do.").format(bzbug.id))
        return
    except Exception as e:
        print("-> does not exist yet...")

    attachment_metadata = get_attachments_metadata(bzbug)
    # some comments include shell formating tokens like ESC[31m breaking the xml parser,
    # let's just forget them with a warning message
    try:
        comments = bzbug.getcomments()
    except Exception as e:
        print("!! PARSING ERROR OF COMMENTS...")
        comments = [{'text':'An error occurred while importing comments, please check the original bugzilla entry using the link above.',
                     'creation_time': bzbug.creation_time,
                     'creator': bzbug.creator}]

    firstcomment = None if len(comments) < 1 else comments[0]
    desctext = None
    if firstcomment and 'author' in firstcomment:
        author = firstcomment['author']
    elif firstcomment and 'creator' in firstcomment:
        author = firstcomment['creator']
    else:
        author = None
    if author == bzbug.creator:
        desctext = firstcomment['text']
        if 'attachment_id' in firstcomment:
            desctext += '\n' + migrate_attachment(firstcomment,
                                                  attachment_metadata)
        comments = comments[1:]

    description = \
        template.render_issue_description(bzurl, bzbug, desctext, user_cache, hg2git)

    labels = ['bugzilla']

    if bzbug.status in STATUS_MAP:
        labels += [STATUS_MAP[bzbug.status]]

    if bzbug.component.lower() not in ('general', '.general', target.product):
        l = COMPONENT_MAP.get(bzbug.component, None)
        if l is not None:
            labels.append(l)
        else:
            labels.append(format(bzbug.component.title()))

    if 'Core' in bzbug.component:
        labels += ['core']

    if bzbug.severity in SEVERITY_MAP:
        labels += [SEVERITY_MAP[bzbug.severity]];

    if bzbug.priority in PRIORITY_MAP:
        labels += [PRIORITY_MAP[bzbug.priority]];

    weight = -1
    if bzbug.priority in PRIORITY2WEIGHT:
        weight = PRIORITY2WEIGHT[bzbug.priority]

    for kw in bzbug.keywords:
        if kw in KEYWORD_MAP:
            labels += [KEYWORD_MAP[kw]]

    milestone = None
    bz_milestone = bzbug.target_milestone
    if (not bz_milestone) or (bz_milestone == '---'):
        # print(bzbug.blocks)
        if 25 in bzbug.blocks:
            bz_milestone = '3.0'
        if 55 in bzbug.blocks:
            bz_milestone = '3.1'
        if 387 in bzbug.blocks:
            bz_milestone = '3.2'
        if 558 in bzbug.blocks:
            bz_milestone = '3.3'
        if 814 in bzbug.blocks:
            bz_milestone = '3.4'
        if 1683 in bzbug.blocks:
            bz_milestone = '3.5'
        if 814 in bzbug.blocks:
            bz_milestone = '3.4'
        if 331 in bzbug.blocks:
            bz_milestone = '4.0'
        if 1608 in bzbug.blocks:
            bz_milestone = '3.x'
    if bz_milestone and bz_milestone != '---':
        milestone = milestone_cache[bz_milestone]

    if user_cache[bzbug.creator]:
        sudo = user_cache[bzbug.creator].id
    else:
        sudo = None

    try:
        issue = target.create_issue(bzbug.id, bzbug.summary, description,
                                    labels, milestone,
                                    str(bzbug.creation_time), weight
                                    #sudo=sudo
                                    )
    except Exception as e:
        print("!! create issue failed once...")
        # try to by-pass SPAM detector
        try:
            issue = target.create_issue(bzbug.id, spam_workaround(bzbug.summary), spam_workaround(description),
                                        labels, milestone,
                                        str(bzbug.creation_time), weight
                                        )
        except Exception as e:
            print("!! create issue failed twice...")

            labels += ["import failure"]
            issue = target.create_issue(bzbug.id, "Bugzilla imported bug {}".format(bzbug.id),
                                        "**Error while importing [bugzilla bug {id}]({link_url}/show_bug.cgi?id={id})**  \n".format(id=bzbug.id,link_url=bzurl),
                                        labels, milestone,
                                        str(bzbug.creation_time), weight
                                        )

            print("bzbug.summary:")
            print(spam_workaround(bzbug.summary))
            print("bzbug.description:")
            print(spam_workaround(description))
            print("==========")

    # Assign bug to actual account if exists
    assignee = user_cache[bzbug.assigned_to]
    if assignee and assignee.id is not None:
        issue.assignee_id = assignee.id

    print("Migrating comments: ")
    c = 0
    duplicate_of = None
    for comment in comments:
        c = c + 1
        print("  [{}/{}]".format(c, len(comments)))
        comment_attachment = ""
        # Only migrate attachment if this is the comment where it was created
        if 'attachment_id' in comment and \
                comment['text'].startswith('Created attachment'):
            comment_attachment = migrate_attachment(comment,
                                                    attachment_metadata)

        emoji, action, body = analyze_bugzilla_comment(comment,
                                                       attachment_metadata)
        if 'author' in comment:
            if user_cache[comment['author']]:
                author = user_cache[comment['author']].display_name()
                sudo = user_cache[comment['author']].id
            else:
                author = comment['author']
                sudo = None
        else:
            if user_cache[comment['creator']]:
                author = user_cache[comment['creator']].display_name()
                sudo = user_cache[comment['creator']].id
            else:
                author = comment['creator']
                sudo = None
        gitlab_comment = template.render_comment(bzurl, emoji, author, action,
                                                 body, comment_attachment, hg2git)

        try:
            issue.notes.create({
                'body': gitlab_comment
                , 'created_at': str(comment['creation_time'])
            })#, sudo=sudo)
        except Exception as e:
            print("!! create note failed once...")
            time.sleep(10)
            try:
                issue.notes.create({
                    'body': gitlab_comment
                    , 'created_at': str(comment['creation_time'])
                })#, sudo=sudo)
            except Exception as e:
                print("!! create note failed twice...")
                issue.labels += ["import failure"]
                issue.notes.create({
                    'body': "Failed to import comment number {}, please check original bug report.".format(c)
                    , 'created_at': str(comment['creation_time'])
                })
                print("body:")
                print(gitlab_comment)
                print("==========")

        m = re.search('\*\*\* This bug has been marked as a duplicate of bug ([0-9]+) \*\*\*', body)
        if m:
            duplicate_of = m.group(1)

    # Do last, so that previous actions don't all send an email
    for cc_email in itertools.chain(bzbug.cc, [bzbug.creator]):
        subscriber = user_cache[cc_email]
        if subscriber and subscriber.id is not None:
            try:
                issue.subscribe(sudo=subscriber.username)
            except gitlab.GitlabSubscribeError as e:
                if e.response_code in (201, 304):
                    # 201 == workaround for python-gitlab bug
                    # https://github.com/python-gitlab/python-gitlab/pull/382
                    # 304 == already subscribed
                    continue
                if e.response_code == 403:
                    print("WARNING: Subscribing users requires admin. "
                          "Subscribers will not be migrated.")
                    break
                raise e

    if duplicate_of is not None:
        try:
            issue.notes.create({
                    'body': ('\duplicate #{}').format(duplicate_of)
                })
        except Exception as e:
            print("!! error while creating dup note {}".format(bzbug.id))
            issue.labels += ["import failure"]

    try:
        if bzbug.status in ('CLOSED','RESOLVED'):
            print("create closed notes...")
            if bzbug.resolution in ('INVALID','WONTFIX','WORKSFORME'):
                issue.notes.create({
                    'body': 'This issue has been closed as ' + bzbug.resolution + '.'
                })
            issue.state_event = 'close'
            issue.save()
            print("create closed notes OK")
        else:
            # Workaround python-gitlab bug by providing redundant state_event
            # https://github.com/python-gitlab/python-gitlab/pull/389
            print("save reopen...")
            issue.save(state_event='reopen')
            print("save reopen OK")
    except Exception as e:
        print("!! error while saving {}".format(bzbug.id))
        issue.labels += ["import failure"]

    print("New GitLab issue created from bugzilla bug "
          "{}: {}".format(bzbug.id, issue.web_url))

    if bzbug.bugzilla.logged_in:
        bz = bzbug.bugzilla
        print("Adding a comment in bugzilla and closing the bug there")
        # TODO: Create a resolution for this specific case? MIGRATED or FWDED?
        bz.update_bugs(bzbug.bug_id, bz.build_update(
            comment=template.render_bugzilla_migration_comment(instance, issue),
            status='RESOLVED',
            resolution=resolution))


def options():
    parser = argparse.ArgumentParser(description="Bugzilla migration helper for https://eigen.tuxfamily.org/bz")
    parser.add_argument('--automate', action='store_true',
                        help="don't wait on user input and answer \'Y\' (yes) \
                              to any question")
    parser.add_argument('--token', help="gitlab token API", required=True)
    parser.add_argument('--product', help="bugzilla product name", required=True)
    parser.add_argument('--component', help="bugzilla component name")
    parser.add_argument('--bz-user', help="bugzilla username")
    parser.add_argument('--bz-password', help="bugzilla password")
    parser.add_argument('--target-project', metavar="USERNAME/PROJECT",
                        help="project name for gitlab, like 'username/project'.", required=True)
    parser.add_argument('--hg2git', help="path to hg2git-* translation files")
    return parser.parse_args()


def check_if_target_project_exists(target):
    try:
        project = target.get_project()
    except Exception as e:
        print("ERROR: Could not access the project `{}` - are you sure \
               it exists?".format(target.target_project))
        print("You can use the --target-project=username/project option if \
               the project name\n\is different from the Bugzilla \
               product name.")
        exit(1)

    if not project.issues_enabled:
        print("Enabling issues for GitLab project")
        project.issues_enabled = 1
        project.save()


def main():
    args = options()

    glurl = "https://gitlab.com/"
    bzurl = "https://eigen.tuxfamily.org/bz"
    #giturl = "https://gitlab.com/ggael/eigen-tmp"
    giturl = "https://gitlab.inria.fr/guenneba/eigen-bzmigration-test2"
    instance = "gitlab.com"
    bzresolution = 'MOVED'

    target = common.GitLab(glurl, giturl, args.token, args.product,
                           args.target_project, args.automate)

    target.connect()

    if args.target_project is not None:
        check_if_target_project_exists(target)

    print("Connecting to %s" % bzurl)
    if args.bz_user and args.bz_password:
        bgo = bugzilla.Bugzilla(bzurl+"/xmlrpc.cgi", args.bz_user, args.bz_password)
    else:
        print("WARNING: Bugzilla credentials were not provided, BZ bugs won't "
              "be closed and subscribers won't notice the migration")
        bgo = bugzilla.Bugzilla(bzurl+"/xmlrpc.cgi", tokenfile=None)

    query = bgo.build_query(product=args.product, component=args.component)
    if args.component:
        print("Querying for all bugs for the '%s' product, '%s' component" %
              (args.product, args.component))
    else:
        print("Querying for all bugs for the '%s' product, all components" %
              args.product)

    bzbugs = bgo.query(query)
    print("{} bugs found".format(len(bzbugs)))
    count = 0
    
    if args.hg2git is not None:
        print("Load hg-to-git hash maps from %s" % args.hg2git)
    
    hg2git = common.Hg2Git(args.hg2git)

    error_count = 0

    # There are products without Bugzilla tracking
    if len(bzbugs) != 0:
        milestone_cache = milestones.MilestoneCache(target)
        user_cache = users.UserCache(target, bgo, args.product)

        # TODO: Check if there were bugs from this module already filed (i.e.
        # use a tag to mark these)
        for bzbug in bzbugs:
            count += 1
            
            time.sleep(1)
            sys.stdout.write('[{}/{}] '.format(count, len(bzbugs)))
            try:
                processbug(bgo, bzurl, instance, bzresolution, target, user_cache,
                           milestone_cache, bzbug, hg2git)
            except Exception as e:
                print("\n!! ERROR PROCESSING BUG {}".format(bzbug.id))
                print(e)
                print("\n")
                error_count+=1
                if error_count>20:
                    return -1
            

        # do a second pass to update internal links
        print('Update #XXX links...')
        count = 0
        for bzbug in bzbugs:
            time.sleep(1)
            count += 1
            sys.stdout.write('[{}/{}] '.format(count, len(bzbugs)))
            issue = target.get_project().issues.get(bzbug.id)
            if re.match(r'#[0-9]+:\n', issue.description):
                issue.description = issue.description+' '
            # TODO do the same for all comments
            issue.save()

    if os.path.exists('users_cache'):
        print('IMPORTANT: Remove the file \'users_cache\' after use, it contains sensitive data')


if __name__ == '__main__':
    main()
